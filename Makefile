all: index.html

index.html: README.md
	markdown -o $@ $<

publish:
	git add index.html README.md
	git commit -sm "$(shell date +%Y%m%d%H%M%S)"
	git push

clean:
	rm -f index.html

.PHONY: all publish clean
