# What is this?

**pf-kernel** is a desktop-oriented Linux kernel fork. Its name is by **no means** related to BSD Packet Filter. "pf" stands for "post-factum", the author's nickname.

Join official **#pfkernel** channel @ Libera.Chat IRC and official Matrix room: [**#pf-kernel:envs.net**](https://matrix.to/#/#pf-kernel:envs.net).

# OK, what's there in your patchset?

Currently, pf-kernel incorporates:

* [stable kernel update](https://git.kernel.org/cgit/linux/kernel/git/stable/linux-stable.git)
* [graysky's GCC patch](https://github.com/graysky2/kernel_compiler_patch)
* [userspace-assisted KSM](https://codeberg.org/pf-kernel/uksmd)
* [v4l2loopback device](https://github.com/umlaeute/v4l2loopback)
* [BBRv2](https://github.com/google/bbr/commits/v2alpha)
* [zstd v1.5.4](https://github.com/facebook/zstd/releases/tag/v1.5.4)
* [AMD EPP P-state driver](https://lore.kernel.org/lkml/20230131090016.3970625-1-perry.yuan@amd.com/) with [guided autonomous mode](https://lore.kernel.org/lkml/20230307112740.132338-1-wyes.karny@amd.com/) support
* [DDCCI driver](https://gitlab.com/ddcci-driver-linux/ddcci-driver-linux)
* [CPU parallel bringup](https://lore.kernel.org/lkml/20230308171328.1562857-1-usama.arif@bytedance.com/)
* random fixes here and there

This improves interactiveness and performance, saves some memory and fixes nasty bugs.

# How do I get it?

The full list of all the releases, including the latest one, [is available here](https://codeberg.org/pf-kernel/linux/releases).

## But I want binary builds!

There are Arch Linux official binaries available for some architectures (_64-bit only_):

* [generic x86\_64](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-generic)
* [x86\_64\_v2](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-generic-v2)
* [x86\_64\_v3](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-generic-v3)
* [x86\_64\_v4](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-generic-v4)
* [Intel Core 2](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-core2)
* [Intel Silvermont](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-silvermont)
* [Intel Skylake](https://build.opensuse.org/package/show/home:post-factum:kernels/linux-pf-skylake)

### Where is Fedora, Gentoo etc?

Some unofficial packages are maintained here:

* [Gentoo](https://packages.gentoo.org/packages/sys-kernel/pf-sources)

Please don't ask me to fix or update them, I just host some links. But if you know that some other distributions also support pf-kernel, let me know.

#### I saw linux-pf package in AUR, is it supported?

No, since it is maintained by other people. But I still leave some links to it here:

* [linux-pf](https://aur.archlinux.org/packages/linux-pf) with some enhancements
* [linux-pf-git](https://aur.archlinux.org/packages/linux-pf-git) to be on the very edge

# I have an idea for you!

Please [let me know](mailto:oleksandr@natalenko.name).

# Reminder

As per [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html), this program is distributed in the hope that it will be useful, but **WITHOUT ANY WARRANTY**; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Please keep this in mind.
